<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\FeedbackVotes;

/**
  * Class for handling votes of feedbacks
  */
 class FeedbackVoteHandler extends Model
 {
 	public $model; //model for save votes
 	public $idField; // unique field of id for use id_feedback or id_comment in mysql queries
 	public function __construct()
 	{
 		$this->model = new FeedbackVotes();
 		$this->idField = 'id_feedback';
 	}
 	/*It's method need for use  in the extended class, 
 	and not use assigment the unique property id_feedback in the extended class P.S.
 	I know it's not good solve, but i can't find another*/  
 	protected function assigmentAdd($idUser, $typeVote = null)
 	{
 		$this->model->id_user = $idUser;
 		$this->model->type_vote = $typeVote;
 	}
 	//add a new unique vote
 	public function addVote($idRecord, $idUser, $typeVote = null)
 	{
 		$this->model->id_feedback = $idRecord;
 		$this->assigmentAdd($idUser, $typeVote);
 		return $this->model->save() ? $this->model : null;
 	}
 	//delete  vote of a feedback 
 	public function deleteVote($idRecord)
 	{
 		$this->model = $this->model::find()->where([$this->idField => $idRecord])->one();
 		 ($this->model!=null) ? $this->model->delete() : '';
 	}
 	//change  vote of a feedback 
 	public function updateVote($idRecord, $typeVote)
 	{
 		$this->model = $this->model::find()->where([$this->idField => $idRecord])->one();
 		if($this->model!=null) {
 		  $this->model->type_vote = $typeVote;
 		  $this->model->save();
 		}
 	}
 	
 } 