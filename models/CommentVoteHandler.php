<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\FeedbackVoteHandler;
use app\models\CommentVotes;
/**
  * Class for handling votes of comments
  */
 class CommentVoteHandler extends FeedbackVoteHandler
 {
 	public function __construct()
 	{
 		$this->model = new CommentVotes();
 		$this->idField = 'id_comment';
 	}

 	public function addVote($idRecord, $idUser, $typeVote = null)
 	{
 		$this->model->id_comment = $idRecord;
 		parent::assigmentAdd($idUser, $typeVote);
 		return $this->model->save() ? $this->model : null;
 	}
 } 