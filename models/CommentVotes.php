<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * 
 */
class CommentVotes extends ActiveRecord
{
	public function getComment()
	{
		return $this->hasOne(Comment::className(), ['id' => 'id_comment']);
	}
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'id_user']);
	}
}