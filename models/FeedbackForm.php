<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Feedback;
use app\models\BaseForm;
/**
 * The model behind the feedback forms.
 */
class FeedbackForm extends BaseForm
{
	public $view = 'feedback';
	
	public function insertMessage()
	{
		$this->model = new Feedback();
		parent::insertMessage();
		//save changes or return null
		return $this->model->save() ? $this->model : null;
	}
}
