<?php

namespace app\models;

use Yii;
use app\models\Comment;
/**
 * CommonForm is the model behind the feedback and comment forms.
 */
class CommentForm extends BaseForm
{
	public $view = 'comment';
	public $feed_back_id;
	//saving the message to the data base
	public function insertMessage()
	{
		$this->model = new Comment();
		parent::insertMessage();
		//assigning values
		$this->model->feed_back_id = $this->feed_back_id;
		//save changes or return null
		return $this->model->save() ? $this->model : null;
	}
}
