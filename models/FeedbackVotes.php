<?php
namespace app\models;

use yii\db\ActiveRecord;

/**
 * 
 */
class FeedbackVotes extends ActiveRecord
{
	public function getComment()
	{
		return $this->hasOne(Feedback::className(), ['id' => 'id_feedback']);
	}
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'id_user']);
	}
}