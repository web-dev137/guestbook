<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Feedback;
/**
 * CommonForm is the model behind the feedback and comment forms.
 */
abstract class BaseForm extends Model
{
	public $content;
	public $image;
	public $model;
	
	public function rules()
	{
		return [
			['content', 'trim'], //delte spaces before and after content
			['content', 'required'], //this field must be not empty
			['content', 'string', 'max'=>500], // set type and length of field
			['image', 'file', 'extensions' => 'png, jpg'], // set type "file" and valid file extension
		];
	}
	//save image
	public  function saveImg($folder)
	{
		$path_img = "uploads/{$folder}/{$this->image->baseName}.{$this->image->extension}";
		$this->image->saveAs($path_img);
		return $path_img;
	}

	public function insertMessage()
	{
		//check for valid models and fields of the form
		if(!$this->validate()) {
			return null;
		}
		//assigning values
		$this->model->content = $this->content;
		$this->model->id_user = Yii::$app->user->id;
		$this->model->time = date("h:m:s d/m/y");
		if($this->image)
		$this->model->img_path = $this->saveImg($view);
	}
}
