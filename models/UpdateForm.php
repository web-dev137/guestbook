<?php
	namespace app\models;
	use yii\base\Model;
	use app\models\FeedBack;
	use app\models\Comment;

	/**
	 * 
	 */
	class UpdateForm extends Model
	{
		private $nameModel;
		public $content;
		function __construct($nameModel)
		{
			$this->nameModel = $nameModel;
		}
		public function rules()
		{
			return [
				['content', 'required'],
				['content', 'string', 'min' => 2, 'max' => 250],
			];
		}
		public function updateRecord($id)
		{
			if(!$this->validate()){
				return null;
			}
			$model = ($this->nameModel == 'Feedback') ?
			Feedback::findOne($id) : Comment::findOne($id);
			$model->content = $this->content; 
			return $model->save() ? $model : null;
		}
	}