<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class Feedback extends ActiveRecord
{
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'id_user']);
	}
	public function getComments()
	{
		return $this->hasMany(Comment::className(), ['feed_back_id' => 'id']);
	}
}
