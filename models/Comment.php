<?php
	
namespace app\models; 

use Yii;
use yii\db\ActiveRecord;

/**
 * 
 */
class Comment extends ActiveRecord
{
	public function getFeedBack()
	{
		return $this->hasOne(FeedBack::className(), ['id' => 'feed_back_id']);
	}
}