<?php
namespace app\repository;

use app\repository\VoteInterface;
use yii\db\Query;
use app\models\FeedbackVotes;
/**
 * 
 */
class FeedbackRepository 
{
	public $field = 'id_feedback';
	public $table = 'feedback';
	public $voteTable = 'feedback_votes';
	public  function getAll()
	{
		$allRecords = (new Query())
        ->select("{$this->table}.id, {$this->table}.id_user, username,time,content, img_path")
        ->from($this->table)
        ->join('INNER JOIN', 'user', "user.id = {$this->table}.id_user");
        return $allRecords;
	}
	public  function findById($id)
	{
		return $this->getAll()->where(['=', "$this->table.id", $id]);
	}
		public  function getCountVotes($idRecord, $typeVote)
	{ 
		$res= (new Query())
		->select('*')
		->from($this->voteTable)
		->where([
			'and',
			['=', $this->field, $idRecord],
			['=', 'type_vote', $typeVote]
		]);
		return $res->count();
	}

	public  function findVoteUser($idUser, $idRecord)
	{ 
		$res = (new Query())
		->select('*')
		->from($this->voteTable)
		->where([
			'and',
			['=', $this->field, $idRecord],
			['=', 'id_user', $idUser]
		])
		->one();
		return $res;
	}

}