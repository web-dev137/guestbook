<?php
namespace app\repository;

use yii\db\Query;
use app\repository\FeedbackRepository;
use app\models\CommentVotes;
/**
 * 
 */
class CommentRepository extends FeedbackRepository
{
	public $field = 'id_comment';
	public $table = 'comment';
	public $voteTable = 'comment_votes';
	//return object query contaning all recoeds
	public  function getAllRecords($feed_back_id)
	{
		return parent::getAll()
		->where(['=','feed_back_id',$feed_back_id]);
	}
	// return quantity of comments for each feedback
	public  function getCountByIdFeedBack($feed_back_id)
	{
		return $this->getAllRecords($feed_back_id)->count();
	}
	
}