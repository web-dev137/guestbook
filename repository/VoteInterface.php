<?php
namespace app\repository;
/**
*
**/
interface VoteInterface {
	public static function getCountVotes($idRecord, $typeVote);
	public static function findVoteUser($idUser, $idRecord);
}