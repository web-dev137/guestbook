<?php

namespace app\widgets;

use Yii;
use yii\helpers\Html;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;

/**
 * 
 */
class ShowImg
{
	
	public static function showImg($imgPath)
	{
		if($imgPath) {
			$img = Yii::getAlias($imgPath);
			Image::getImagine()->open($img)->thumbnail(new Box(120, 120))->save($img, ['quality' => 100]);
			echo Html::img('@web/'.$imgPath, ['alt' => 'img']);
		}
	}
}