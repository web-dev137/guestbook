<?php
 
 namespace app\widgets;

use Yii;
use yii\web\ForbiddenHttpException;
use yii\helpers\Html;
 /**
  * 
  */
 class Votes
 {
 	//generate a link for votes
 	public static function addVote($id_record, $typeVote, $action, $counterVotes)
 	{
 		//action in site controller can't to recognize the guest
 		 $isGuest = \Yii::$app->user->isGuest; 
 		$nameTypeVote = ($typeVote) ? 'like' : 'dislike';
 		echo 
		 	Html::a(
			            Html::img('image/'.Html::encode($nameTypeVote).'.png',
			                [
			                	'alt' => Html::encode($nameTypeVote), 
			                	'class' => 'vote_img'
			            	]),
				            [	
				            	"site/{$action}", 
				                'idRecord' => Html::encode($id_record), 
				                'typeVote' => Html::encode($typeVote), 
				            	'isGuest' => Html::encode($isGuest)
			            	],
			        );
		 	 	echo "<span class='counterVoites'>".$counterVotes."</span>";
	}
}