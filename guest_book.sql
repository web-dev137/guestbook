-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Апр 01 2021 г., 07:42
-- Версия сервера: 5.7.15
-- Версия PHP: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `guest_book`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE `comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `feed_back_id` int(10) UNSIGNED NOT NULL,
  `content` varchar(250) NOT NULL,
  `img_path` varchar(100) DEFAULT NULL,
  `time` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `id_user`, `feed_back_id`, `content`, `img_path`, `time`) VALUES
(1, 2, 2, 'I use this smartphone already two years. He never let me down.\r\n', NULL, '11:36 19/01/2021'),
(13, 3, 10, 'The characters were worse than the stated ones.', NULL, '17:01:56 20/01/21'),
(16, 3, 25, 'He was broken after two months of use.\r\n', NULL, '05:01:16 26/01/21'),
(17, 3, 27, 'Not super but for everyday use it is suitable', NULL, '05:01:18 26/01/21'),
(29, 4, 29, 'What is this?', NULL, '02:01:26 28/01/21'),
(30, 3, 26, 'He has all what i need for my work', NULL, '05:01:52 31/01/21'),
(36, 2, 2, 'That is smartphone', 'uploads/comments/smartfon_zte_blade_a3_blue_1098779_5.png', '06:01:47 31/01/21'),
(37, 3, 27, 'I say about this headphones', 'uploads/comments/headphones.jpg', '06:01:20 31/01/21'),
(38, 3, 25, '45t5', NULL, '04:02:06 01/02/21'),
(39, 3, 34, 'his oportunity have no limit', NULL, '04:02:33 01/02/21'),
(40, 4, 34, 'My congratulations for you', NULL, '04:02:50 01/02/21'),
(41, 3, 37, 'the games give out a very impressive FPS output', NULL, '04:02:20 02/02/21'),
(42, 3, 37, 'Even on ultra sets!', NULL, '04:02:37 02/02/21'),
(43, 3, 25, 'bdf', NULL, '05:02:34 02/02/21'),
(44, 4, 41, 'It is great!', NULL, '06:02:41 03/02/21'),
(45, 3, 25, 'That  smartphone', 'uploads//smartfon_zte_blade_a3_blue_1098779_5.png', '06:02:06 03/02/21'),
(46, 4, 42, 'It\'s sad', NULL, '05:03:06 29/03/21'),
(47, 3, 44, 'I have been using it for 4 years', NULL, '02:03:30 30/03/21'),
(48, 3, 45, 'I mean the redmi 9C smartphone', NULL, '02:03:56 30/03/21');

-- --------------------------------------------------------

--
-- Структура таблицы `comment_votes`
--

CREATE TABLE `comment_votes` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_comment` int(10) UNSIGNED NOT NULL,
  `type_vote` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comment_votes`
--

INSERT INTO `comment_votes` (`id`, `id_user`, `id_comment`, `type_vote`) VALUES
(1, 4, 34, '0'),
(2, 3, 39, '1'),
(5, 4, 36, '1');

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--

CREATE TABLE `feedback` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `content` varchar(500) NOT NULL,
  `img_path` varchar(100) DEFAULT NULL,
  `time` varchar(19) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `feedback`
--

INSERT INTO `feedback` (`id`, `id_user`, `content`, `img_path`, `time`) VALUES
(2, 2, 'It\'s a good smartphone!!!', '', '05:01:18 18/01/21'),
(26, 3, 'This notebook was best from what i see before!', NULL, '10:01:38 25/01/21'),
(27, 3, 'It were not bad headphones', NULL, '05:01:59 26/01/21'),
(35, 3, 'I was best smartphone', 'uploads/feed_back/smartfon_xiaomi_redmi_note_8_pro_6_128gb_blue_1215836_4.png', '05:01:50 31/01/21'),
(42, 3, 'It was bad smatphone', 'uploads//smartfon_zte_blade_a3_blue_1098779_5.png', '06:02:13 03/02/21'),
(43, 3, 'I was best keyboard', NULL, '06:02:43 03/02/21'),
(44, 3, 'I was best keyboard', NULL, '02:03:56 30/03/21'),
(45, 3, 'I was best smartphone', NULL, '02:03:27 30/03/21');

-- --------------------------------------------------------

--
-- Структура таблицы `feedback_votes`
--

CREATE TABLE `feedback_votes` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_feedback` int(11) NOT NULL,
  `type_vote` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `feedback_votes`
--

INSERT INTO `feedback_votes` (`id`, `id_user`, `id_feedback`, `type_vote`) VALUES
(154, 3, 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1611124818),
('m140506_102106_rbac_init', 1611126601),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1611126601),
('m180523_151638_rbac_updates_indexes_without_prefix', 1611126602),
('m200409_110543_rbac_update_mssql_trigger', 1611126602);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `group` varchar(10) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` varchar(19) NOT NULL,
  `updated_at` varchar(19) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `group`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `created_at`, `updated_at`) VALUES
(2, 'Vasiliy', 'authUser', 'GJGhZvzvUN73dWSGBKtT2uX0eScMED7W', '$2y$13$KoN8WW8tR9R5tJ0ikrrOpei4keLYdk97FnhxkVhs.UsCRsiRMfCaW', NULL, 'Vasya@mail.ru', '18/01/2021 05:15:15', NULL),
(3, 'Petr', 'authUser', 'GvWm0yvSJOFGqdN2Z_lcSaRavkjyI6b9', '$2y$13$Xwv7f/Tpts7fyN84MNQQqePtrZZHLpHMXJU7kRDLFSsp2aGeN9Bva', NULL, 'Petr@mail.ru', '19/01/2021 05:42:32', NULL),
(4, 'Alexandr', 'admin', 'E6BH911WRV7hAvv6LHqFoLQvc5iiZC1I', '$2y$13$Zti8aoxMR2JZJ0uTlfOEy.Sr2GagJGGd/DGOc1znWX/eFFVkLXbyC', NULL, 'Sanya@mail.ru', '20/01/2021 03:52:29', NULL);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `feed_back_id` (`feed_back_id`);

--
-- Индексы таблицы `comment_votes`
--
ALTER TABLE `comment_votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_comment` (`id_comment`);

--
-- Индексы таблицы `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Индексы таблицы `feedback_votes`
--
ALTER TABLE `feedback_votes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_feedback` (`id_feedback`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT для таблицы `comment_votes`
--
ALTER TABLE `comment_votes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT для таблицы `feedback_votes`
--
ALTER TABLE `feedback_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
