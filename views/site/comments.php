<?php
	use yii\bootstrap\ActiveForm;
	use yii\helpers\Url;
	use yii\helpers\Html;
	use app\widgets\ShowImg;
	use app\widgets\Votes;
	use app\repository\CommentRepository;
?>
<div class="container">
	<div class="row text-center">
        <?= $feed_back['username'] ?>
    	<?= $feed_back['time'] ?>
    </div>
    <div class="row text-center">
        <?= $feed_back['content'] ?>
    </div>
</div>
<div class="container">
	<?php foreach ($models as $model) { ?>
		<?= '<div class="row">'.$model['username'] ?>
		<?= ' '.$model['time'].'</div>' ?>
		<?= '<div class="row">'.$model['content'].'</div>'; ?>
		<div class="row">
		<?= ShowImg::showImg($model['img_path']); ?>
		</div>
		<div class="row">
		<?php 
			Votes::addVote(
				$model['id'], 1, 'comment_vote',
				 (new CommentRepository())->getCountVotes($model['id'], 1)
			);
			Votes::addVote(
				$model['id'], 0, 'comment_vote',
				 (new CommentRepository())->getCountVotes($model['id'], 0)
			);
		?>
		</div>
		<div class="row">
		<?php
			if(Yii::$app->user->identity->group == 'admin'){
				Url::remember();
				echo
				 Html::tag('div',
						Html::a('Редактировать', 
							['admin/update', 'id' => $model['id'], 'nameModel' => 'Comment'])
		                	.' '.
						Html::a('Удалить',
		                	 ['admin/delete', 'id' => $model['id'], 'nameModel' => 'Comment']),
		                	  ["class" => "row"]
		                );
			}
		?>
		</div>
	<?php } ?>
</div>
<div class="container">
	<?php   $form = ActiveForm::begin(); ?>
	<?=		$form->field($commentForm, 'content') ?>
	<?=		$form->field($commentForm, 'image')->fileInput() ?>		
	<?=		$form->field($commentForm, 'feed_back_id')->hiddenInput()->label(false) ?>
	<div> 
		<?= Html::submitButton('отправить', 
			[
				'class'=>'btn btn-primary', 
				'name' => 'send-btn'
			]);
		?>
	</div>
	<?php ActiveForm::end(); ?>
</div>