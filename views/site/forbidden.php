<?php
use Yii;
use yii\web\ForbiddenHttpException;
?>
<div class="container">
	<div class="row text-center">
		<?php throw new ForbiddenHttpException('Access denied'); ?>
	</div>

