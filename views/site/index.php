<?php
 use yii\helpers\Html;
 use yii\helpers\Url;
 use yii\bootstrap\ActiveForm;
 use app\repository\FeedbackRepository;
 use app\repository\CommentRepository;
 use app\models\Vote;
 use app\widgets\ShowImg;
 use app\widgets\Votes;
 use yii\widgets\LinkPager;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="container">

        <?php 
            foreach ($models as $model) {
        ?>
        <div class="row">
            <?= $model['username'] ?>
        </div>
        <div class="row">
            <?= $model['time'] ?>
        </div>
        <div class="row">
            <?= $model['content'] ?>
        </div>
        <div class="row">
            <?= ShowImg::showImg($model['img_path']); ?>
        </div>
        <?php
         
          
            Votes::addVote($model['id'], 1, 'feedback_vote', (new FeedbackRepository())->getCountVotes($model['id'], 1)
        );
            
            Votes::addVote($model['id'], 0, 'feedback_vote', (new FeedbackRepository())->getCountVotes($model['id'], 0)
        );
        ?>
        
        <div class="row">
        <?php 
            $count = (new CommentRepository())->getCountByIdFeedBack($model['id']);
            $count = ($count > 0) ? $count : ''; 
            $action = ($count > 0) ? 'Смотреть комментарии' : 'Комментировать';
            ?>
            <?=
             Html::a($action.' '.$count, 
                        ['site/comments', 
                        'feed_back_id'       => $model['id'],
                    ]); 
            ?>
          <?php
            if(Yii::$app->user->identity->group == 'admin'){
                Url::remember();
                echo
                   Html::tag('div',
                        Html::a('Редактировать',
                             ['admin/update', 'id' => $model['id'], 'nameModel' => 'Feedback']).' '.
                        Html::a('Удалить',
                             ['admin/delete', 'id' => $model['id'], 'nameModel' => 'Feedback']),
                              ["class" => "row"
                        ]);
                }
            ?>
        </div>
        <?php } ?>
        
    </div>
    
    <?php 
           $form = ActiveForm::begin(['options'=> ['enctype' => 'multipart/form-data']]); ?>
              <?= $form->field($modelForm,'content'); ?>
              <?= $form->field($modelForm, 'image')->fileInput() ?>
              <div class="form-group">
                    <?= Html::submitButton('Оставить отзыв', ['class' => 'btn btn-primary', 'name' => 'send-button']) ?>
            </div>
            <?php  ActiveForm::end(); ?>
    <?php 
        echo 
            '<div class = "row">'
                .'<div class = "pull-right">'
                    .LinkPager::widget([
                        'pagination' => $pages,
                    ])
                .'</div>'
            .'</div>';
    ?>
</div>
