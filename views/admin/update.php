<?php
  use yii\helpers\Html;
  use yii\bootstrap\ActiveForm;
?>
  <div class="container">
    <h1><?= Html::encode($this->title); ?></h1>
    <p>Edit record:</p>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-lg-push-3 col-md-push-3">
          <?php 
           $form = ActiveForm::begin(['id'=> 'form-edit-record']); ?>
              <?= $form->field($updateForm,'content')->textarea(['rows' => 6]) ?>
              <div class="form-group">
                    <?= Html::submitButton('Редактировать запись', ['class' => 'btn btn-primary', 'name' => 'edit-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
      </div>
  </div>