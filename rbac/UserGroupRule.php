<?php
namespace app\rbac;

use Yii;
use yii\rbac\Rule;

/**
 * This class is responsible for checking for equality of 
 * the role of the current user, 
 * the role specified in the permissions array
 */
class UserGroupRule extends Rule
{
	public $name = 'userGroup';

	public function execute($user, $item, $params)
	{
		if(!\Yii::$app->user->isGuest) { //checking for a guest
			$group = \Yii::$app->user->identity->group;
			//checking for an authorized user or an administrator 
			if ($item->name === 'admin') {
				return $group == 'admin';
			}
			elseif ($item->name === 'authUser') {
				return $group == 'authUser';
			}
		}
		return false;
	}
}