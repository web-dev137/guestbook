<?php
namespace app\rbac;

use Yii;
use yii\rbac\Rule;
use yii\rbac\Item;
/**
 * This class is responsible for checking for equality 
 * of the index of the owner of the feedback the index
 * of the current user or equality of the role of the current user
 * the role administrator 
 */
class UserFeedBackOwnerRule  extends Rule
{
	public $name = 'isFeedBackOwner';

	public function execute($user, $item, $params)
	{
		
		$group = Yii::$app->user->identity->group;
		//checking for a guest 
		if(!\Yii::$app->user->isGuest) {
			if ($group == 'admin' || $group == 'authUser') {
				/* return true if the owner feedback is the current user
				 or administrator else return fase*/
				return 	isset($params['id_owner_feedBack']) ? 
					(\Yii::$app->user->identity->id 
					== $params['id_owner_feedBack'] 
					|| $group == 'admin') ? true : false 
				: false;
			} else {
				return false;
			}
		} else{
			return false;
		}
		
	}
}