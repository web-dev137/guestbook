<?php

return [
    'login' => [
        'type' => 2,
    ],
    'logout' => [
        'type' => 2,
    ],
    'signup' => [
        'type' => 2,
    ],
    'index' => [
        'type' => 2,
    ],
    'comments' => [
        'type' => 2,
        'children' => [
            'addOwnComment',
        ],
    ],
    'delete' => [
        'type' => 2,
    ],
    'update' => [
        'type' => 2,
    ],
    'feedback_vote' => [
        'type' => 2,
    ],
    'comment_vote' => [
        'type' => 2,
    ],
    'addOwnComment' => [
        'type' => 2,
        'description' => 'Добавить комментарий к своему отзыву',
        'ruleName' => 'isFeedBackOwner',
    ],
    'guest' => [
        'type' => 1,
        'children' => [
            'login',
            'logout',
            'signup',
            'index',
            'comments',
        ],
    ],
    'authUser' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'feedback_vote',
            'comment_vote',
            'addOwnComment',
        ],
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'userGroup',
        'children' => [
            'delete',
            'update',
        ],
    ],
];
