<?php
namespace app\controllers;

use Yii;
//use yii\web\UrlManager;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use yii\db\Query;
use app\models\Feedback;
use app\models\Comment;
use app\models\UpdateForm;
/**
 * 
 */
class AdminController extends Controller
{
	/**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
 /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /*
        Update  comments or feedback record
    */
    public function actionUpdate($id, $nameModel)
    {
    	if($nameModel == 'Feedback') {
    		$nameModel = 'Feedback';
    	} elseif($nameModel == 'Comment') {
    		$nameModel = 'Comment';
    	} else {
    		$this->redirect('../views/site/error.php');
    	}
           $url = Url::previous();
    	$model = ($nameModel == 'Feedback') ? Feedback::findOne($id)
            : Comment::findOne($id);
        $updateForm = new UpdateForm($nameModel);
        $updateForm->content = $model->content;
        if($updateForm->load(Yii::$app->request->post()) 
            && $updateForm->updateRecord($id))
        {
        	return Yii::$app->getResponse()->redirect($url);
        }
        return $this->render('update', [
        	'updateForm' => $updateForm,  
        ]);
    }
    /*
        delete feedback or comment record
    */
	public function actionDelete($id, $nameModel)
    {
         if(!\Yii::$app->user->can('delete')) {
            throw new ForbiddenHttpException('Access denied');
            }
        $model = ($nameModel == 'Feedback') ? Feedback::findOne($id)
            : Comment::findOne($id);
        $model->delete();
        $this->redirect(Yii::$app->request->referrer ?: $this->goBack());
    }
}