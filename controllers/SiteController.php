<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use yii\data\Pagination;
use yii\db\Query;
use app\models\LoginForm;
use app\repository\FeedbackRepository;
use app\repository\CommentRepository;
use app\models\Feedback;
use app\models\FeedbackForm;
use app\models\User;
use app\models\Comment;
use app\models\CommentForm;
use app\models\FeedbackVotes;
use app\models\CommentVotes;
use app\models\FeedbackVoteHandler;
use app\models\CommentVoteHandler;
use app\models\SignupForm;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    /** 
     * this action checking all permission and rules before call of main actions
     */
    function beforeAction($action)
    {
        if(parent::beforeAction($action)) {
                if(!\Yii::$app->user->can($action->id)) {
                    Yii::$app->getResponse()->redirect('../view/site/forrbidden.php');
                }
                return true;
        } else {
            return false;
        }
    }
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
  
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $group = \Yii::$app->user->identity->group;
        //to add any feedback can only authorized user  not admin and guest
        $deniedPermission = ($group == 'admin') || Yii::$app->user->isGuest;
        $models = (new FeedbackRepository())->getAll();
        //set params for pagination 
        $pages = new Pagination(['totalCount' => $models->count(), 'pageSize' => 5]);
       $modelForm = new FeedbackForm();
        if ($modelForm->load(Yii::$app->request->post())){ 
            if(!$deniedPermission){
                $modelForm->image = UploadedFile::getInstance($modelForm, 'image');
                $modelForm->insertMessage((new Feedback()));
                return $this->refresh();
            } else {
                throw new ForbiddenHttpException('Access denied');
            }
        }
        return $this->render('index',[
            'models' => $models
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->all(),
            'modelForm' => $modelForm,
            'pages' => $pages
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    /*
        show coments and processes the form data 
    */
    public function actionComments($feed_back_id)
    {
        $models = (new CommentRepository())->getAllRecords($feed_back_id)->all();
        $feedBack = (new FeedbackRepository())->findById($feed_back_id)->one();
        $commentForm = new CommentForm();
        $commentForm->feed_back_id = $feedBack['id'];
        //the rule - each user can comment on own feedback 
        $rule = \Yii::$app->user->can('addOwnComment',
                ['id_owner_feedBack' => $feedBack['id_user']]);

        if($commentForm->load(Yii::$app->request->post())) {
            if($rule) {
                $commentForm->image = UploadedFile::getInstance($commentForm, 'image');
                $commentForm->insertMessage((new Comment()));
                return $this->refresh(); 
            } else {
                throw new ForbiddenHttpException('Access denied');
            }
        }
        return $this->render('comments', [
            'models' => $models,
            'feed_back' => $feedBack,
            'commentForm' => $commentForm,
            'feed_back_id' => $feed_back_id 
        ]);
    }
    /*
        processes requests to add, remove and change vote
    */
    public function actionFeedback_vote($idRecord, $typeVote, $isGuest)
    {
        $idUser = Yii::$app->user->identity->id;
        if($isGuest){
            throw new ForbiddenHttpException('Access denied'); // only for authorized users
        }
          $model = new FeedbackVotes();
          $vote = new FeedbackVoteHandler();
          $voteUser =  (new FeedbackRepository())->findVoteUser($idUser, $idRecord);
        if($voteUser == null) {
            $vote->addVote($idRecord, $idUser, $typeVote);
        } else {
            ($voteUser["type_vote"] == $typeVote) ? 
            $vote->deleteVote($idRecord) :  
             $vote->updateVote($idRecord, $typeVote);    
         }
         $this->redirect(Yii::$app->request->referrer ?: $this->goBack());
    }
    public function actionComment_vote($idRecord, $typeVote, $isGuest)
    {
        
        $idUser = Yii::$app->user->identity->id;
        if($isGuest){
            throw new ForbiddenHttpException('Access denied'); // only for authorized users
        }
          $model = new CommentVotes(); // CommentVotes or FeedbackVotes
        $vote = new CommentVoteHandler();
       /*
            check isset or not vote of the user
       */
       $voteUser = (new CommentRepository())->findVoteUser($idUser, $idRecord);
        /* 
            if record of user's vote isset then to add the vote
            else we check type of vote(like/dislike) and if they are different 
            then we to update this vote else we delete the record. After these actions
            we redirect the user to the home page
        */
        if($voteUser == null) {
            $vote->addVote($idRecord, $idUser, $typeVote);
        } else {
            ($voteUser["type_vote"] == $typeVote) ? 
            $vote->deleteVote($idRecord) :  
             $vote->updateVote($idRecord, $typeVote);    
         }
             $this->redirect(Yii::$app->request->referrer ?: $this->goBack());
        
    }

  
}
