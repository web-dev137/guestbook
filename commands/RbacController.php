<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\rbac\UserGroupRule;
use app\rbac\UserFeedBackOwnerRule;
/**
 * 
 */
class RbacController extends Controller
{
	
	public function actionInit()
	{
		//get object authManager
		$authManager = \Yii::$app->authManager;
		
		//create roles
		$guest 	= $authManager->createRole('guest');
		$authUser 	= $authManager->createRole('authUser');
		$admin	= $authManager->createRole('admin');

		// create permissions
		$login 			= $authManager->createPermission('login');
		$logout 		= $authManager->createPermission('logout');
		$signup 		= $authManager->createPermission('signup');
		$index 			= $authManager->createPermission('index');
		$comments		= $authManager->createPermission('comments');
		$feedback_vote	= $authManager->createPermission('feedback_vote');
		$comment_vote	= $authManager->createPermission('comment_vote');
		$delete			= $authManager->createPermission('delete');
		$update			= $authManager->createPermission('update');

		//adding all permissons in authManager 
		$authManager->add($login);
		$authManager->add($logout);
		$authManager->add($signup);
		$authManager->add($index);
		$authManager->add($comments);
		$authManager->add($delete);
		$authManager->add($update);
		$authManager->add($feedback_vote);
		$authManager->add($comment_vote);

		//create object with the rule for group
		$userGroupRule = new UserGroupRule();
		$authManager->add($userGroupRule);

		//add rule in roles
		$authUser->ruleName 	= $userGroupRule->name;
		$admin->ruleName 	= $userGroupRule->name;

		/*
			Create rule for  authorized users. Each authorized user except the administrator
			can add comment to  only the own comment, administrator can add comment to any feedback
		*/
		$userFeedBackOwnerRule = new UserFeedBackOwnerRule();
		$authManager->add($userFeedBackOwnerRule);
		$addOwnComment = $authManager->createPermission('addOwnComment');
		$addOwnComment->description = 'Добавить комментарий к своему отзыву';

		$addOwnComment->ruleName 	= $userFeedBackOwnerRule->name;
		$authManager->add($addOwnComment);

		//add roles in authManager
		$authManager->add($guest);
		$authManager->add($authUser);
		$authManager->add($admin);

		/*
			add permission and rule per role in authManager
		*/
		//add rule to permisson to extend the rule to all roles
		$authManager->addChild($comments, $addOwnComment);
		$authManager->addChild($guest, $login);
		$authManager->addChild($guest, $logout);
		$authManager->addChild($guest, $signup);
		$authManager->addChild($guest, $index);
		$authManager->addChild($guest, $comments);
		
		//user
		$authManager->addChild($authUser, $feedback_vote);
		$authManager->addChild($authUser, $comment_vote);
		$authManager->addChild($authUser, $addOwnComment);
		
		//admin
		$authManager->addChild($admin, $delete);
		$authManager->addChild($admin, $update);
	}
}